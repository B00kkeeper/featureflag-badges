import os

projects = {
    'DEV': {
        'project_url': 'https://gitlab.com/B00kkeeper/featureflag-dev/-/feature_flags',
        'token': os.getenv('DEV_API_TOKEN'),
        'project_id': '58449458'
        },
    'UAT':{
        'project_url': 'https://gitlab.com/B00kkeeper/featureflag-uat/-/feature_flags',
        'token': os.getenv('UAT_API_TOKEN'),
        'project_id': '58451581'
        },
    'QA':{
        'project_url': 'https://gitlab.com/B00kkeeper/featureflag-qa/-/feature_flags',
        'token': os.getenv('QA_API_TOKEN'),
        'project_id': '58451505'
        },
    'PROD':
    {
        'project_url': 'https://gitlab.com/B00kkeeper/featureflags-prod/-/feature_flags',
        'token': os.getenv('PROD_API_TOKEN'),
        'project_id': '58451654'
    }
}