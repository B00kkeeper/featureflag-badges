import json
import os
import requests

from config import projects

def get_flags(project):
    projectId = project['project_id']
    headers = {'Private-Token': project['token'], 'Content-Type': 'application/json'}
    response = requests.get(f'https://gitlab.com/api/v4/projects/{projectId}/feature_flags', headers=headers)

    if response.status_code != 200:
        print(f'Request failed with status code {response.status_code}')
        exit()
    
    return json.loads(response.text)

def build_bage(project_name, project, flag_name, flag_value):
    bage = f'https://img.shields.io/badge/{flag_name}-N/A-lightgrey'

    if flag_value is not None:
        value = 'ON' if flag_value else 'OFF'
        color = 'green' if flag_value else 'red'
        bage = f'https://img.shields.io/badge/{flag_name}-{value}-{color}'

    return f'[![{project_name}]({bage})]({project["project_url"]})'

project_names = list(projects.keys())

new_content = "**Feature Flags** | " + " | ".join([f'**{name}**' for name in project_names])
new_content += "\n--- | " + " | ".join(["---" for _ in project_names])\

project_flags = {}
flag_names = set()

for projectName, project in projects.items():   

    feature_flags = get_flags(project)  

    project_flags[projectName] = {flag['name']: flag['active'] for flag in feature_flags}
    for flag in feature_flags:
        if flag['name'] not in flag_names:
            flag_names.add(flag['name'])

for flag_name in flag_names:
    new_content += f'\n**{flag_name}** | ' + " | ".join([f'{build_bage(project_name, project, flag_name, project_flags[project_name].get(flag_name))}' for project_name, project in projects.items()])

personal_token  = os.getenv('REPOSITORY_TOKEN')
file_url = f"https://gitlab.com/api/v4/projects/{os.getenv('CI_PROJECT_ID')}/repository/files/README.md"


headers = {
    "PRIVATE-TOKEN": personal_token,
    "Content-Type": "application/json"
}

payload = {
    "branch": "main",  # Replace with the appropriate branch name
    "commit_message": "Update README with Latest FeatureFlags changes",
    "content": new_content
}

response = requests.put(file_url, json=payload, headers=headers)

if response.status_code == 200:
    print("README.md updated successfully!")
else:
    print(f"Error updating README.md: {response.status_code} - {response.text}")


